Description
===========

JSONv is a python library designed to validate JSON documents from formal grammars. It is an equivalent of DTDs for the XML language.

Installation
============

	pip install jsonv

Documentation
=============

JSON format (from [json.org](http://www.json.org/))
---------------------------

There are two kinds of structures in JSON: objects (aka dictionaries) and arrays.

An **object** is an unordered set of name/value pairs. An object begins with **{** (left brace) and ends with **}** (right brace). Each name is followed by **:** (colon) and the name/value pairs are separated by **,** (comma):

![alt tag](http://www.json.org/object.gif)

An **array** is an ordered collection of values. An array begins with **[** (left bracket) and ends with **]** (right bracket). Values are separated by **,** (comma):

![alt tag](http://www.json.org/array.gif)

A **value** can be a string in double quotes, or a number, or true or false or null, or an object or an array. These structures can be nested:

![alt tag](http://www.json.org/value.gif)

Grammar syntax
--------------

JSONv use context-free grammars in which every production rule is of the form:

	RULE_NAME ::= w

where w is a regular expression or a terminal.

A JSONv grammar has to contain an **entry point**:

``` javascript 
{
	entry: "<regexp>",
	...
}
```

There are two kinds of rules:

* **Nonterminal rules**: they are used to describe a JSON object or a JSON array. Use the following syntax:

``` javascript
"RULE_NAME": {
	"rule": "pair|value",
	"key": "<json key>",
	"type": "object|array",
	"contains": "<regexp>",
}
```

where the pair ```contains``` containts a regular expression to describe the content of the JSON object|array, the pair ```rule``` indicates whether the rule matches to a pair in a JSON object or value in a JSON array. In the first case only, the JSON key have to be specified with the pair ```key```. Finally, the pair ```type``` indicates the content type i.e. ```object|array```.

Example (see [demo](#markdown-header-demo)):
``` javascript
{
	"menus": [
		...
	]
}
```
can be described with:
``` javascript
{
	"entry": "MENUS",

	"MENUS": {
		"rule": "pair",
		"key": "menus",
		"type": "array",
		"contains": "...",
	},
	...
}
```

---

* **Terminal rules**: they are used to describe a JSON value (string, number, boolean). Use the following syntax:

``` javascript
"RULE_NAME": {
	"rule": "pair|value",
	"key": "<json key>",
	"type": "object|array",
}
```

where the pair ```rule``` indicates whether the rule matches to a pair in a JSON object or value in a JSON array. In the first case only, the JSON key have to be specified with the pair ```key```. Finally, the pair ```type``` indicates the content type i.e. ```str|flt|bool```.

Example (see [demo](#markdown-header-demo)):
``` javascript
"id": "my_id"
```
can be described with:
``` javascript
"MENU_ID": {
	"rule": "pair",
	"key": "id",
	"type": "str",
}
```

---

The JSONv regular expressions are built from rule names and the following operators:

Operator | Description
-------- | -----------
```,```   | The comma operator matches the expression before and the expression after the operator. It ignores the order, ```A,B``` is equivalent to ```B,A```.
```│```   | The vbar operator matches either the expression before or the expression after the operator. It ignores the order, ```A│B``` is equivalent to ```B│A```.
```?```   | The question mark indicates there is zero or one of the preceding element.
```+```   | The plus sign indicates there is one or more of the preceding element.
```*```   | The asterisk indicates there is zero or more of the preceding element.
```( )``` | Groups a series of pattern elements to a single element.

Example (see [demo](#markdown-header-demo)):
``` javascript
"contains": "MENU_ID, MENU_VALUE, MENU_ICON?, MENUS?"
```

---

Demo
====

This basic demo shows how to describe and validate a menu bar in JSON.

``` python
import jsonv.JsonParser
import jsonv.JsonValidator

json = '''
{
	"menus": [{
	"icon": [255, 255, 255, 255],
	"id": "file",
	"caption": "File",
	"menus": [
		{ "id": "new", "caption": "New" },
		{ "id": "open", "caption": "Open" },
		{ "id": "save", "caption": "Save" },
	  ]
	}]
}
'''

gram = '''
{
	"entry": "MENUS",

	"MENUS": {
		"rule": "pair",
		"key": "menus",
		"type": "array",
		"contains": "MENU*",
	},
	"MENU": {
		"rule": "value",
		"type": "object",
		"contains": "MENU_ID, MENU_VALUE, MENU_ICON?, MENUS?",
	},
	"MENU_ID": {
		"rule": "pair",
		"key": "id",
		"type": "str",
	},
	"MENU_VALUE": {
		"rule": "pair",
		"key": "caption",
		"type": "str",
	},
	"MENU_ICON": {
		"rule": "pair",
		"key": "icon",
		"type": "array",
		"contains": "MENU_ICON_PIXEL*",
	},
	"MENU_ICON_PIXEL": {
		"rule": "value",
		"type": "flt",
	},
}
'''

try:
	document = jsonv.JsonParser.parseString(json)

except jsonv.JsonParser.JsonParserError, e:
	print('%s' % e.__str__())

else:
	try:
		validator = jsonv.JsonValidator.parseString(gram)

		if validator.validate(document):
			print('The document is valid')
		else:
			print('The document is not valid')

	except jsonv.JsonValidator.JsonValidatorError, e:
		print('%s' % e.__str__())
```
